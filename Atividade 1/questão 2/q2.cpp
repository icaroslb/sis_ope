#include <iostream>
#include <ctime>
#include <chrono>
#include <thread>
#include <vector>

#define NUM_DE_THREADS 4
#define DISTANCIA 10

char *cores[] = {"\x1B[34m","\x1B[35m","\x1B[36m","\x1B[33m"};

void corredor (int indice, int dist) {
        int percorrido = 0, descanso;
        srand(time(NULL));
        
        while (dist > percorrido) {
            std::cout << cores[indice] << "Faltam " << dist - percorrido << " metros para o corredor " << indice << "\x1B[0m"<< std::endl;
            percorrido++;
            descanso = (rand () % 6) + 1;
            
            std::cout << cores[indice] << "Corredor " << indice << " está descansando por " << descanso << " min" << std::endl;
            std::this_thread::sleep_for(std::chrono::duration<int>(descanso));
        }
        
        std::cout << "\x1B[32mCorredor " << indice << " chegou!!!\x1B[0m" << std::endl;
}

int main (int argc, char *argv[]) {
    std::vector<std::thread> threads;
    
    for (int i = 0; i < NUM_DE_THREADS; i++) {
        threads.push_back(std::thread(corredor, i, DISTANCIA));
    }
    
    for (int i = 0; i < NUM_DE_THREADS; i++) {
        threads[i].join();
    }

    return 0;
}
