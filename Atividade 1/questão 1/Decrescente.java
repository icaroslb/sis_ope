import java.util.Random;

class Decrescente extends Thread {
    int id;

    Decrescente (int id) {
        this.id = id;
    }

    public void run () {
        Random r = new Random();

        for (int i = 100; i >= 0; i--) {
            try {
                Thread.sleep(1000 * r.nextInt(5));
             } catch (Exception e) {
                System.out.println(e);
             }

            System.out.println("T" + id + ": " + i);
        }
    }
}