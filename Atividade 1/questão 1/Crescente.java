import java.util.Random;

public class Crescente implements Runnable {
    int id;

    Crescente (int id) {
        this.id = id;
    }

    public void run () {
        Random r = new Random();
        for (int i = 0; i <= 100; i++) {
            try {
                Thread.sleep(1000 * r.nextInt(5));
             } catch (Exception e) {
                System.out.println(e);
             }

            System.out.println("T" + id + ": " + i);
        }
    }
}