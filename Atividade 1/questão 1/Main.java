public class Main {
    public static void main(String[] args) {
        Crescente c1 = new Crescente(Integer.parseInt(args[0])),
                  c2 = new Crescente(Integer.parseInt(args[1]));
        Decrescente d1 = new Decrescente(Integer.parseInt(args[2])),
                     d2 = new Decrescente(Integer.parseInt(args[3]));

        new Thread(c1).start();
        new Thread(c2).start();
        new Thread(d1).start();
        new Thread(d2).start();
    }
}