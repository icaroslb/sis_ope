class Deposito extends Thread {
    int idConta;
    float val;
    Banco banco;

    Deposito (int idConta, float val, Banco banco) {
        this.idConta = idConta;
        this.val = val;
        this.banco = banco;
    }

    public void run () {
        banco.depositar(idConta, val);
        int saldoAtualizado = banco.saldoDisponivel(idConta);

        System.out.println("Depósito de "+saldoAtualizado+" na conta "+idConta+" realizado com sucesso!");
        System.out.println("["+idConta+" - "+saldoAtualizado+"]");
    }
}