import java.util.Iterator;
import java.util.Vector;

class Banco {
    Vector contas;

    Banco () {}

    public float saldoDisponivel (int id) {
        return contas[id].saldo();
    }

    public void depositar (int id, float val) {
        contas[id].depositar(val);
    }

    public void sacar (int id, float val) {
        contas[id].sacar(val);
    }
}