class Transferência extends Thread {
    int idRemetente, idDestinatario;
    float val;
    Banco banco;

    Transferência (int idRemetente, int idDestinatario, float val, Banco banco) {
        this.idRemetente = idRemetente;
        this.val = val;
        this.banco = banco;
    }

    public void run () {
        if (val <= banco.saldoDisponivel(idRemetente)) {
            banco.sacar(idRemetente, val);
            banco.depositar(idDestinatario, val);
            int saldoRemetenteAtualizado = banco.saldoDisponivel(idRemetente),
                saldoDestinatarioAtualizado = banco.saldoDisponivel(idDestinatario);

            System.out.println("Transferência de "+saldoRemetenteAtualizado+" da conta "+idRemetente+" para a "+idDestinatario+" realizado com sucesso!");
            System.out.println("["+idRemetente+" - "+saldoRemetenteAtualizado+"] --> ["+idRemetente+" - "+saldoDestinatarioAtualizado+"]");
        } else {
            System.out.println("Transferência de "+saldoRemetenteAtualizado+" da conta "+idRemetente+" para a "+idDestinatario+" falhado!");
            System.out.println("["+idRemetente+" - "+saldoRemetenteAtualizado+"] --> ["+idRemetente+" - "+saldoDestinatarioAtualizado+"]");
        }
    }
}