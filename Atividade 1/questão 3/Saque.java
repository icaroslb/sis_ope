class Saque extends Thread {
    int idConta;
    float val;
    Banco banco;

    Saque (int idConta, float val, Banco banco) {
        this.idConta = idConta;
        this.val = val;
        this.banco = banco;
    }

    public void run () {
        if (val <= banco.saldoDisponivel(idConta)) {
            banco.sacar(idConta, val);
            int saldoAtualizado = banco.saldoDisponivel(idConta);

            System.out.println("Saque de "+saldoAtualizado+" da conta "+idConta+" realizado com sucesso!");
            System.out.println("["+idConta+" - "+saldoAtualizado+"]");
        } else {
            System.out.println("Saque de "+saldoAtualizado+" da conta "+idConta+" falhado!");
            System.out.println("["+idConta+" - "+saldoAtualizado+"]");
        }
    }
}