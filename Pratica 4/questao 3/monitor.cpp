#include <iostream>
#include <thread>
#include <string>
#include <mutex>
#include <random>
#include <chrono>
#include <string>
#include <condition_variable>
#include <vector>

bool agora = false;
std::mutex escrevendo,
           ir_dormir;
std::vector<bool> dormindo;
std::vector<int> estado_filosofo;

enum{PENSANDO, COM_FOME, COMENDO};

void print (int estado, int id) {
    escrevendo.lock();
    switch (estado) {
        case PENSANDO:
            std::cout << "Filosofo " << id << " está pensando!" << std::endl;
        break;
        case COM_FOME:
            std::cout << "Filosofo " << id << " está com fome!" << std::endl;
        break;
        case COMENDO:
            std::cout << "Filosofo " << id << " está comendo!" << std::endl;
        break;
    }
    escrevendo.unlock();
}

void acordar (int id) {
    ir_dormir.lock();
    dormindo[id] = false;
    ir_dormir.unlock();
}

class Monitor {
    std::vector <std::mutex> garfos;
    std::vector <std::thread*> filosofos;

public:
    Monitor () : garfos(5), filosofos(5) { }

    void inserirFilosofo (int id, std::thread *filosofo) {
        filosofos.push_back(filosofo);
    }

    bool pegar (int id) {
        return (estado_filosofo[id] == COM_FOME) && (try_lock(garfos[id], garfos[(id + 1) % 5]) == -1);
    }

    void libera (int id) {
        garfos[id].unlock();
        garfos[(id + 1) % 5].unlock();

        if (pegar((id + 1) % 5)) {
            acordar((id + 1) % 5);
        }

        if (pegar((id + 4) % 5)) {
            acordar((id + 4) % 5);
        }        


    }
};

class Filosofo {
    Monitor *mon;
    int id;

public:
    Filosofo (int id, Monitor *mon) : id(id), mon(mon) {}

    void fazer () {
        std::random_device rd;
        int tempo;
        while (!agora) {
            std::this_thread::yield();
        }

        while (true) {
            //Pensando
            estado_filosofo[id] = PENSANDO;
            print(PENSANDO, id);
            std::this_thread::sleep_for(std::chrono::milliseconds(rd() % 10000));
            //Com fome
            estado_filosofo[id] = COM_FOME;
            print(COM_FOME, id);
            if (!mon->pegar(id)) {
                dormir();
            }
            //Comendo
            estado_filosofo[id] = COMENDO;
            print(COMENDO, id);
            std::this_thread::sleep_for(std::chrono::milliseconds(rd() % 10000));
            mon->libera(id);
        }
    }

    void dormir () {
        ir_dormir.lock();
        dormindo[id] = true;
        ir_dormir.unlock();
        while (dormindo[id]) {
            std::this_thread::yield();
        }
    }
};

int main () {
    Monitor monitor;
    std::vector <Filosofo> filosofos;
    std::vector <std::thread> threads;
    std::cout << '\n';
    for (int i = 0; i < 5; i++) {
        filosofos.push_back(Filosofo(i, &monitor));
        dormindo.push_back(false);
        estado_filosofo.push_back(PENSANDO);
    }

    for (int i = 0; i < 5; i++) {
        threads.push_back(std::thread(&Filosofo::fazer, &filosofos[i]));
    }

    agora = true;

    while (true) {}
}