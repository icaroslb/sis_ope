#include <iostream>
#include <thread>
#include <string>
#include <mutex>
#include <random>
#include <chrono>

#define NUMERO_INDISPONIVEL 1
#define PAR true
#define IMPAR false

class DropBox {
    int numero;
    bool ePar, nlido;
    std::mutex lerNumero;

public:
    DropBox () : nlido(false) {}
    int pegar (const bool classificacao) {
        lerNumero.lock();
        if (classificacao != ePar || !nlido) {
            lerNumero.unlock();
            throw(NUMERO_INDISPONIVEL);
        } else {
            std::string tipo = (classificacao == PAR) ? "par" : "impar";
            std::cout << "Consumidor " << tipo << " obtem " << numero << std::endl;
            nlido = false;
            lerNumero.unlock();
            return numero;  
        }
    }

    void colocar (int novoNumero) {
        lerNumero.lock();
        if (nlido) {
            lerNumero.unlock();
            throw(NUMERO_INDISPONIVEL);
        } else {
            numero = novoNumero;
            ePar = (numero % 2) == 0;
            nlido = true;

            std::cout << "Produtor gerou " << numero << std::endl;
            lerNumero.unlock();
        }
    }
};

class Produtor {
    DropBox *dropbox;

public:
    Produtor (DropBox *db) : dropbox(db) {}
    void rodar () {
        std::random_device rd;
        int gerado;
        
        while (true) {
            gerado = rd() % 10;
            
            try {
                std::this_thread::sleep_for(std::chrono::milliseconds(rd() % 1000));
                dropbox->colocar (gerado);
            } catch (int erro) { }
        }
    }
};

class Consumidor {
    DropBox *dropbox;
    bool leitura;

public:
    Consumidor (DropBox *db, bool tipo) : dropbox(db), leitura(tipo) {}

    void rodar () {
        std::random_device rd;

        while (true) {
            try {
                std::this_thread::sleep_for(std::chrono::milliseconds(rd() % 1000));
                dropbox->pegar(leitura);
            } catch (int erro) { }
        }
    }
};

int main () {
    std::thread *threads[3];
    DropBox *dp = new DropBox();
    Consumidor c1(dp, PAR),
               c2(dp, IMPAR);
    Produtor p(dp);

    threads[0] = new std::thread(&Consumidor::rodar, &c1);
    threads[1] = new std::thread(&Consumidor::rodar, &c2);
    threads[2] = new std::thread(&Produtor::rodar, &p);

    for (int i = 0; i < 3; i++) {
        threads[0]->join();
    }

    return 0;
}