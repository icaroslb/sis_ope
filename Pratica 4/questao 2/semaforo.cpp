#include <iostream>
#include <thread>
#include <string>
#include <shared_mutex>
#include <mutex>
#include <random>
#include <chrono>
#include <string>
#include <condition_variable>
#include <vector>

std::shared_mutex muTexto;
std::mutex escrevendo;
std::string texto;

void escrever () {
    std::string auxTexto;
        
    while (true) {
        std::cin >> auxTexto;

        muTexto.lock();
            texto = auxTexto;
            std::cout << "\033[1A\033[1000D\033[KEscritor mudou texto!\n\033[K";
        muTexto.unlock();
    }
}


void ler (int id) {
    std::random_device rd;

    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(rd() % 10000));
        muTexto.lock_shared();
            escrevendo.lock();
                system("tput sc");
                std::cout << "\033[s\033[1A\033[1000D\033[K"
                          << "Leitor " << id << " leu: " << texto << "\n\033[u";
                system("tput rc");
            escrevendo.unlock();
        muTexto.unlock_shared();
    }
}

int main () {
    std::thread escritor(escrever);
    std::vector <std::thread> leitores;

    for (int i = 0; i < 10; i++) {
        leitores.push_back(std::thread(ler, i));
    }

    while (true) {}
}