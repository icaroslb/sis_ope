#include <iostream>
#include <thread>
#include <string>
#include <shared_mutex>
#include <mutex>
#include <random>
#include <chrono>
#include <string>
#include <condition_variable>
#include <vector>


class Monitor {
    std::shared_mutex muTexto;
    std::mutex escrevendo;
    std::string texto;

public:
    Monitor () : muTexto(), texto() { }

    void ler (int id) {
        muTexto.lock_shared();
        escrevendo.lock();
        system("tput sc");
        std::cout << "\033[s\033[1A\033[1000D\033[K"
                  << "Leitor " << id << " leu: " << texto << "\n\033[u";
        system("tput rc");
        escrevendo.unlock();
        muTexto.unlock_shared();
    }

    void escrever (std::string novoTexto) {
        muTexto.lock();
        texto = novoTexto;
        std::cout << "\033[1A\033[1000D\033[KEscritor mudou texto!\n\033[K";
        muTexto.unlock();
    }
};

class Escritor {
    Monitor *mon;

public:
    Escritor (Monitor *mon) : mon(mon) {}

    void fazer () {
        std::string texto;
        
        while (true) {
            std::cin >> texto;

            mon->escrever(texto);
        }
    }
};

class Leitor {
    Monitor *mon;
    int id;

public:
    Leitor (Monitor *mon, int id) : mon(mon), id(id) {}

    void fazer () {
        std::random_device rd;

        while (true) {
            std::this_thread::sleep_for(std::chrono::milliseconds(rd() % 10000));
            mon->ler(id);
        }
    }
};

int main () {
    Monitor monitor;
    Escritor escritor (&monitor);
    std::vector <Leitor> leitores;
    std::cout << '\n';
    for (int i = 0; i < 10; i++) {
        leitores.push_back(Leitor(&monitor, i));
    }

    new std::thread(&Escritor::fazer, &escritor);
    for (Leitor &l : leitores) {
        new std::thread(&Leitor::fazer, &l);
    }

    while (true) {}
}