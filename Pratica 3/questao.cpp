#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <random>
#include <queue>
#include <vector>
#include <fstream>

#define COCHILO 5
#define QTDTHREAD 5
#define QTD_TESTES 20

enum {RR, FCFS};

struct Contador {
    int id, turnAround;
    bool terminado;

    Contador (int id) : id(id), turnAround(0), terminado(false) {}
};

int vez = -1,
    threadsTerminaram = 0,
    quantum;
std::mutex inserirFila;
std::vector <Contador*> contadores;
std::queue <Contador*> fila;
Contador *contVez;

void dormir (int tempo) {
    std::this_thread::sleep_for (std::chrono::milliseconds(tempo));
}

void parar (int id) {
    while (vez != id) {
        std::this_thread::yield();
    }
}

void contar (int qtd, Contador &cont) {
    auto inicio = std::chrono::high_resolution_clock::now();
    parar(cont.id);
    for (int i = 0; i < qtd; i++) {
        dormir(COCHILO);
        parar(cont.id);
    }
    cont.turnAround = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - inicio).count();
    
    threadsTerminaram++;
    cont.terminado = true;
}

void roundRobin () {
    while (QTDTHREAD > threadsTerminaram) {
        if (fila.size() > 0) {
            inserirFila.lock();
            contVez = fila.front();
            fila.pop();
            inserirFila.unlock();
            vez = contVez->id;
            auto inicio = std::chrono::steady_clock::now();
            while (!contVez->terminado && (std::chrono::steady_clock::now() - inicio).count() < quantum) {
                std::this_thread::yield();
            }
            
            if (!(contVez->terminado)){
                inserirFila.lock();
                fila.push(contVez);
                inserirFila.unlock();
            } else {
                contadores.push_back(contVez);
            }
        } else {
            std::this_thread::yield();
        }
    }
}

void firstComeFirstService () {
    while (QTDTHREAD > threadsTerminaram) {
        if (fila.size() > 0) {
            inserirFila.lock();
            contVez = fila.front();
            fila.pop();
            inserirFila.unlock();
            vez = contVez->id;
            while (!contVez->terminado) {
                std::this_thread::yield();
            }

            contadores.push_back(contVez);
        } else {
            std::this_thread::yield();
        }
    }
}

int main () {
    std::vector <std::thread> threadsCriadoras;
    std::random_device tempo;

    auto fabricaThread = [] (int id, int qtd, int tempoGerado, std::queue <Contador*> &fila) {
                            Contador *cont = new Contador(id);

                            std::this_thread::sleep_for (std::chrono::milliseconds(tempoGerado));
                            
                            std::thread t(contar, qtd, std::ref(*cont));
                            t.detach();
                            
                            inserirFila.lock();
                            fila.push(cont);
                            inserirFila.unlock();
                         };
    
    for (int k = 0; k < QTD_TESTES; k++) {
        threadsTerminaram = 0;
        vez = -1;
        if (k % 10 == 0) {
            quantum = (tempo() % 400) + 100;
        } 

        for (int i = 0; i < QTDTHREAD; i++) {
            threadsCriadoras.push_back(std::thread(fabricaThread, i, ((100*i) + 100), (tempo() % 500), std::ref(fila)));
        }

        for (std::thread &tc : threadsCriadoras) {
            tc.detach();
        }

        if (k < QTD_TESTES - 10) {
            roundRobin();
        } else {
            firstComeFirstService();
        }
        
        while (fila.size() > 0) {
            contadores.push_back(fila.front());
            fila.pop();
        }
        
        if (k < QTD_TESTES - 10) {
            std::cout <<  "--------- " << (k % 10) + 1 << " - RR ---------" << std::endl
                      << "Quantum: " << quantum << " ms" << std::endl;
        } else {
            std::cout <<  "-------- " << (k % 10) + 1 << " - FCFS --------" << std::endl
                      << "Quantum: - " << std::endl;
        }

        for (Contador *c : contadores) {
            std::cout << "Turn Around " << c->id << " - " << c->turnAround << " ms" << std::endl;
        }

        threadsCriadoras.clear();
        for (Contador *c : contadores) {
            delete(c);
        }
        contadores.clear();
    }

    return 0;
}