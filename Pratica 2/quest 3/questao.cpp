#include <iostream>
#include <thread>
#include <mutex>
#include <vector>

std::mutex escrita;

class ContaBancaria {
    int id;
    float valor;
public:
    ContaBancaria (int id) : id(id), valor(100.f) {}
    int obterId() { return id; }

    float quantidade () { return valor; }
    
    void deposito (float depo) { valor += depo; }
    
    void saque (float saq) { if (valor >= saq) {
                                valor -= saq;
                             } else {
                                 throw(1);
                             }
                           }
};

class Banco {
    std::vector <ContaBancaria> contas;

public:
    Banco() : contas() {}
    int criarConta () { contas.push_back(ContaBancaria(contas.size()));
                        return contas.size() - 1;
                      }
    
    bool deposito (int id, float valor) { try {
                                            contas[id].deposito(valor);
                                            escrita.lock();
                                            std::cout << "Depósito de " << valor << " em " << id << " realizado com sucesso!" << std::endl;
                                            escrita.unlock();
                                            return true;
                                          } catch (int erro) {
                                            escrita.lock();
                                            std::cout << "ID inexistente!" << std::endl;
                                            escrita.unlock();
                                            return false;
                                          }
                                        }
    
    bool saque (int id, float valor) { try {
                                          contas[id].saque(valor);
                                          escrita.lock();
                                          std::cout << "Saque de " << valor << " em " << id << " realizado com sucesso!" << std::endl;
                                          escrita.unlock();
                                          return true;
                                        } catch (int erro) {
                                          if (erro == 1) {
                                            escrita.lock();
                                            std::cout << "Valor indisponível!" << std::endl;
                                            escrita.unlock();
                                          } else {
                                            escrita.lock();
                                            std::cout << "ID inexistente!" << std::endl;
                                            escrita.unlock();
                                          }
                                          return false;
                                        }
                                     }
    
    bool transferencia (int idRemetente, int idDestinatario, float valor) { bool sacou = false;
                                                                            try {
                                                                              contas[idRemetente].saque(valor);
                                                                              sacou = true;
                                                                              contas[idDestinatario].deposito(valor);
                                                                              escrita.lock();
                                                                              std::cout << "Transferência de " << valor << " de " << idRemetente << " para " << idDestinatario << " realizado com sucesso!" << std::endl;
                                                                              escrita.unlock();
                                                                              return true;
                                                                            } catch (int erro) {
                                                                              if (erro == 1) {
                                                                                escrita.lock();
                                                                                std::cout << "Valor indisponível!" << std::endl;
                                                                                escrita.unlock();
                                                                              } else if (sacou) {
                                                                                contas[idRemetente].deposito(valor);
                                                                                escrita.lock();
                                                                                std::cout << "ID destinatário inexistente!" << std::endl;
                                                                                escrita.unlock();
                                                                              } else {
                                                                                escrita.lock();
                                                                                std::cout << "ID remetente inexistente!" << std::endl;
                                                                                escrita.unlock();
                                                                              }
                                                                              return false;
                                                                            }
                                                                          }
    void mostrarSaldos () {
      for (ContaBancaria &cb : contas) {
        std::cout << cb.obterId() << " -- " << cb.quantidade() << std::endl;
      }
    }
};

int main () {
    std::vector<std::thread> threads;
    Banco banco;
    auto depositar = [] (int id, float valor, Banco &banco) { banco.deposito(id, valor); };
    auto sacar = [] (int id, float valor, Banco &banco) { banco.saque(id, valor); };
    auto transferir = [] (int idRemetente, int idDestinatario, float valor, Banco &banco) { banco.transferencia(idRemetente, idDestinatario, valor); };
        

    for (int i = 0; i < 10; i++) {
        banco.criarConta();
    }

    threads.push_back(std::thread(depositar, 0, 100, std::ref(banco)));
    threads.push_back(std::thread(sacar, 0, 200, std::ref(banco)));
    threads.push_back(std::thread(transferir, 3, 2, 1, std::ref(banco)));

    for (std::thread &t : threads) {
        t.join();
    }

    banco.mostrarSaldos();

    return 0;
}