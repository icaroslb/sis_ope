#include <iostream>
#include <thread>
#include <random>
#include <atomic>
#include <mutex>
#include <chrono>
#include <vector>

#define MAX 10
std::atomic<bool> largada(false);
std::mutex escrita;


void preparar () {
    while (!largada) {
        std::this_thread::yield();
    }
}

void dormir (int id) {
    std::random_device tempo;
    unsigned int descanso = (tempo() % 5) + 1;

    escrita.lock();
    std::cout << "Corredor " << id << " está descando por " << descanso << " segundos!" << std::endl;
    escrita.unlock();

    std::this_thread::sleep_for (std::chrono::seconds(descanso));
}

void corredor (int id) {
    preparar();
    for (int i = 0; i < MAX; i++) {
        escrita.lock();
        std::cout << "Faltam " << MAX - i << " metros para o corredor " << id << "!" << std::endl;
        escrita.unlock();
        dormir(id);
    }

    escrita.lock();
    std::cout << "Corredor " << id << " chegou!" << std::endl;
    escrita.unlock();
}

int main () {
    std::vector<std::thread> threads;
    
    threads.push_back(std::thread(corredor, 1));
    threads.push_back(std::thread(corredor, 2));
    threads.push_back(std::thread(corredor, 3));
    threads.push_back(std::thread(corredor, 4));

    largada = true;

    for (std::thread &t : threads) {
        t.join();
    }

    return 0;
}