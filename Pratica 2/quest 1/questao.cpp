#include <iostream>
#include <thread>
#include <random>
#include <atomic>
#include <mutex>
#include <chrono>
#include <vector>

#define MAX 100
std::atomic<bool> largada(false);
std::mutex escrita;


void preparar () {
    while (!largada) {
        std::this_thread::yield();
    }
}

void dormir () {
    std::random_device tempo;
    std::this_thread::sleep_for (std::chrono::seconds((tempo() % 1) + 1));
}

void crescente (int id) {
    preparar();
    for (int i = 0; i < MAX; i++) {
        escrita.lock();
        std::cout << "T" << id << ": " << i << std::endl;
        escrita.unlock();
        dormir();
    }
}

void decrescente (int id) {
    preparar();
    for (int i = MAX; i > 0; i--) {
        escrita.lock();
        std::cout << "T" << id << ": " << i << std::endl;
        escrita.unlock();
        dormir();
    }
}

int main () {
    std::vector<std::thread> threads;
    
    threads.push_back(std::thread(crescente, 1));
    threads.push_back(std::thread(crescente, 2));
    threads.push_back(std::thread(decrescente, 3));
    threads.push_back(std::thread(decrescente, 4));

    largada = true;

    for (std::thread &t : threads) {
        t.join();
    }

    return 0;
}