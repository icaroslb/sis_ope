#!/bin/sh
backUp=$(date +%F-%H) #Pega a data atual
find -cmin -1440 -print | zip $backUp -@ #Procura os arquivos que foram modificados
                                         # a pelo menos 1440 minutos (24h) atrás e
                                         # e comprime os arquivos achados em um zip
                                         # como nome a data atual